# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### Added
- document support for cse8442 digitizer

## [2021.3.0]

### Changed
- major refactor utilizing onboard-averaging firmware

### Added
- new entry-point `yaqd-gage-compuscope-gui <channel_index>`

## [2021.1.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-gage/-/compare/v2021.3.0...master
[2021.3.0]: https://gitlab.com/yaq/yaqd-gage/-/compare/v2021.1.0...2021.3.0
[2021.1.0]: https://gitlab.com/yaq/yaqd-gage/-/tags/v2021.1.0

